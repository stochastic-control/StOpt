ARG IMAGE_REGISTRY=docker.io
ARG IMAGE_NAME=jupyter/minimal-notebook
# Need python 3.8 until ubuntu:22.04
ARG IMAGE_TAG=python-3.8.8

#### Jupyter base
FROM ${IMAGE_REGISTRY}/${IMAGE_NAME}:${IMAGE_TAG} as jupyter_base
ARG PYTHON_VERSION=3.8
ENV PYTHON_VERSION=${PYTHON_VERSION}
RUN python${PYTHON_VERSION} -m pip install numpy scipy matplotlib

#### StOpt Builder
FROM jupyter_base as builder
USER root
ENV DEBIAN_FRONTEND=noninteractive
RUN python${PYTHON_VERSION} -m pip install pybind11-cmake pybind11
RUN apt-get update -qq &&  \
    apt-get install -y \
        gcc \
        cmake \
        libeigen3-dev \
        zlibc \
        zlib1g-dev \
        libbz2-dev \
        libboost-log-dev \
        libboost-thread-dev \
        libboost-test-dev \
        libboost-timer-dev \
        libboost-serialization-dev \
        libboost-random-dev \
        libboost-chrono-dev \
        python3-dev \
        python3-pybind11 \
        pybind11-dev \
        build-essential && \
    rm -rf /var/lib/apt/lists /var/cache/apt/archives
ADD . /tmp/StOpt/
WORKDIR /opt/StOpt
RUN cmake /tmp/StOpt \
    -DPYTHON_LIBRARY=/usr/lib/python${PYTHON_VERSION}/config-${PYTHON_VERSION}-x86_64-linux-gnu/libpython${PYTHON_VERSION}.so
RUN make

#### Runner
FROM jupyter_base as runner
COPY --from=builder /opt/StOpt/ /opt/StOpt/
RUN ln -s /opt/StOpt/lib/* $CONDA_DIR/lib/python${PYTHON_VERSION}/
