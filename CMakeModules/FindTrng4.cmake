# This module finds trng4.
#
# User can give TRNG4_ROOT_DIR as a hint stored in the cmake cache.
#
# It sets the following variables:
#  TRNG4_FOUND              - Set to false, or undefined, if trng4 isn't found.
#  TRNG4_INCLUDE_DIRS       - include directory
#  TRNG4_LIBRARIES          - library files

## config
set(TRNG4_ROOT_DIR "" CACHE PATH "TRNG4 root directory")

if(WIN32)
  if(NOT TRNG4_WIN_VERSION)
    set(TRNG4_WIN_VERSION 125 CACHE STRING "Trng4 version integer code. Necessary on Windows to guess root dir and to determine the library name, i.e. trng4125.lib")
  endif(NOT TRNG4_WIN_VERSION)

  if(NOT TRNG4_WIN_VS_VERSION)
    set(TRNG4_WIN_VS_VERSION 2010 CACHE STRING "Trng4 Visual Studio version, for instance 2008 or 2010.")
  endif(NOT TRNG4_WIN_VS_VERSION)

  if(NOT TRNG4_WIN_LINKAGE)
    set(TRNG4_WIN_LINKAGE mda CACHE STRING "Trng4 linkage variant on Windows. One of these: mda (dll, release), mdd (dll, debug), mta (static, release), mtd (static, debug)")
  endif(NOT TRNG4_WIN_LINKAGE)

  if(NOT TRNG4_WIN_BITNESS)
    set(TRNG4_WIN_BITNESS x64 CACHE STRING "On Windows: x86 or x64 (32bit resp. 64bit)")
  endif(NOT TRNG4_WIN_BITNESS)

  # now, generate platform string
  set(TRNG4_WIN_PLATFORM "${TRNG4_WIN_BITNESS}_windows_vs${TRNG4_WIN_VS_VERSION}/${TRNG4_WIN_LINKAGE}")

else(WIN32)
  set(TRNG4_WIN_PLATFORM "")
endif(WIN32)

## trng4 root dir guessing
# windows: trying to guess the root dir from a 
# env variable set by the trng4 installer
set(WIN_ROOT_GUESS $ENV{TRNG4_STUDIO_DIR${TRNG4_WIN_VERSION}})

FIND_PATH(TRNG4_INCLUDE_DIR
  trng/yarn2.h
  HINTS ${TRNG4_ROOT_DIR}/include
  PATHS ENV C_INCLUDE_PATH
        ENV C_PLUS_INCLUDE_PATH
        ENV INCLUDE_PATH
  )


FIND_LIBRARY(TRNG4_LIBRARY
  NAMES trng4 
  HINTS ${TRNG4_ROOT_DIR}/trng4/lib/${TRNG4_WIN_PLATFORM} #windows
        ${WIN_ROOT_GUESS}/trng4/lib/${TRNG4_WIN_PLATFORM} #windows
        ${TRNG4_ROOT_DIR}/lib
  PATHS ENV LIBRARY_PATH #unix
        ENV LD_LIBRARY_PATH #unix
  )
message(STATUS "TRNG4 Library: ${TRNG4_LIBRARY}")


INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(TRNG4 DEFAULT_MSG 
 TRNG4_LIBRARY TRNG4_INCLUDE_DIR )
	

IF(TRNG4_FOUND)
  SET(TRNG4_INCLUDE_DIRS ${TRNG4_INCLUDE_DIR})
  SET(TRNG4_LIBRARIES ${TRNG4_LIBRARY} )
  IF(CMAKE_SYSTEM_NAME STREQUAL "Linux")
    SET(TRNG4_LIBRARIES "${TRNG4_LIBRARIES};m;pthread")
  ENDIF(CMAKE_SYSTEM_NAME STREQUAL "Linux")
ENDIF(TRNG4_FOUND)

MARK_AS_ADVANCED(TRNG4_LIBRARY TRNG4_INCLUDE_DIR)



