# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import math
import numpy as np

# Defines a specialization of the RegularSpaceGrid  object in one dimension


# define a grid in one dimension
class OneDimRegularSpaceGrid:
    
#     # Default constructor
#     def __init__(self):
#         
#         return None
     
    # Constructor
    # p_lowValue   minimal value of the grid
    # p_step       step size
    # p_nbStep     number of steps
    def __init__(self, p_lowValue, p_step, p_nbStep):
        
        self.m_lowValue = p_lowValue
        self.m_step = p_step
        self.m_nbStep = p_nbStep
        
    # To a coordinate get back the mesh number
    # p_coord   coordinate
    # return mesh number associated to the coordinate
    def getMesh(self, p_coord):
        
        if (self.m_lowValue <= p_coord) == False:
            pass
        
        return np.floor((p_coord - self.m_lowValue) / self.m_step).astype(int)
    
    # get back value
    
    def getLowValue(self):
        
        return self.m_lowValue
    
    def getStep(self):
        
        return self.m_step
    
    def getNbStep(self):
        
        return self.m_nbStep
