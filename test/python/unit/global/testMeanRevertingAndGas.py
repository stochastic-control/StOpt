# Copyright (C) 2016 EDF
# All Rights Reserved
# This code is published under the GNU Lesser General Public License (GNU LGPL)
import math
import numpy as np
import unittest
import StOptGrids
import StOptGlobal
import Utils
import Simulators as sim
import Optimizers as opt

# unit test mean reverting and gas
#################################

class MeanRevertingAndGas(unittest.TestCase):

    def test_(self):
        # grids of times values
        time = StOptGrids.OneDimRegularSpaceGrid(0,1./10,10)
        # values
        values = []
        iPeriod = 52
        for i in range(11):
             values.append(2. + math.sin(math.pi * i * iPeriod) / 10.)
        # future curve
        fut  = Utils.FutureCurve(time,values)
        # mean revering simulator
        sigma = np.zeros(1) + 0.2 # volatility
        mr = np.zeros(1) + 0.05 # mean reverting
        T =1. # maturity
        nbStep = 10 # number of time steps
        nbSimul =100 # number of simulations
        bForward =1 # forward simulation
        r= 0. # interest rate
        simulator = sim.MeanRevertingSimulator(fut,sigma,mr,r,T,nbStep,nbSimul,bForward)
        # get number of simulation
        print(simulator.getNbSimul())
if __name__ == '__main__': 
    unittest.main() 
