// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef NORMALCUMULATIVEDISTRIBUTION_H
#define NORMALCUMULATIVEDISTRIBUTION_H
#define _USE_MATH_DEFINES
#include <math.h>

/** \file NormalCumulativeDistribution.h
 * Calculate
 * \f[
 *     {1 \over { \sqrt{2 \pi}}}  \int_{\infty}^d e^{-x^2/2} dx
 * \f]
 * \author Xavier Warin
 */

/// \class NormalCumulativeDistribution  NormalCumulativeDistribution.h
///  Calculate
///  \f[
///   {1 \over { \sqrt{2 \pi}}}  \int_{\infty}^d e^{-x^2/2} dx
/// \f]
class NormalCumulativeDistribution
{
public :
    /// \brief constructor
    NormalCumulativeDistribution() {};

/// \brief calculate the vale function
/// \param  p_x point where the function is evaluated
/// \return function value;
    double operator()(const double   &p_x) const
    {
        double p = 0.2316419 ;
        double b1 = 0.319381530 ;
        double b2 = -0.356563782 ;
        double b3 = 1.781477937 ;
        double b4 = -1.821255978 ;
        double b5 = 1.330274429 ;
        if (p_x > 0.)
        {

            double t = 1. / (1. + p * p_x) ;
            return (1. - exp(-p_x * p_x / 2.) * t * (b1 + t * (b2 + t * (b3 + t * (b4 + t * b5)))) / (sqrt(2.*M_PI))) ;
        }
        else if (p_x < 0.)
        {
            double t = 1. / (1. - p * p_x) ;
            return exp(-p_x * p_x / 2.) * t * (b1 + t * (b2 + t * (b3 + t * (b4 + t * b5)))) / (sqrt(2.*M_PI)) ;
        }
        else return 0.5 ;
    }
};
#endif
