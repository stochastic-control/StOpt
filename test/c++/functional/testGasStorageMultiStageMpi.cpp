// Copyright (C) 2023 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#define BOOST_TEST_DYN_LINK
#define _USE_MATH_DEFINES
#include <boost/mpi.hpp>
#include <math.h>
#include <memory>
#include <functional>
#include <boost/test/unit_test.hpp>
#include <boost/timer/timer.hpp>
#include <Eigen/Dense>
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "StOpt/regression/LocalLinearRegression.h"
#include "StOpt/core/grids/RegularSpaceGridGeners.h"
#include "StOpt/regression/LocalLinearRegressionGeners.h"
#include "test/c++/tools/simulators/MeanRevertingSimulator.h"
#include "test/c++/tools/dp/DynamicProgrammingByRegressionMultiStageDist.h"
#include "test/c++/tools/dp/DynamicProgrammingByRegressionVaryingGridsMultiStageDist.h"
#include "test/c++/tools/dp/SimulateMultiStageRegressionDist.h"
#include "test/c++/tools/dp/SimulateMultiStageRegressionVaryingGridsDist.h"
#include "test/c++/tools/dp/OptimizeGasStorageMultiStage.h"

using namespace std;
using namespace Eigen ;
using namespace StOpt;


#if defined   __linux
#include <fenv.h>
#define enable_abort_on_floating_point_exception() feenableexcept(FE_DIVBYZERO | FE_INVALID)
#endif

/// For Clang < 3.7 (and above ?) to be compatible GCC 5.1 and above
namespace boost
{
namespace unit_test
{
namespace ut_detail
{
string normalize_test_case_name(const_string name)
{
    return (name[0] == '&' ? string(name.begin() + 1, name.size() - 1) : string(name.begin(), name.size()));
}
}
}
}

double accuracyClose =  2.5;

class ZeroFunction
{
public:
    ZeroFunction() {}
    double operator()(const int &, const ArrayXd &, const ArrayXd &) const
    {
        return 0. ;
    }
};


/// \brief valorization of a given gas storage on a  grid with n period
/// \param p_grid                   the grid
/// \param p_maxLevelStorage       maximum level
/// \param p_nbPeriodInTransition  number of period at each transition step
/// \param p_bOneFile              true if dump in one file
void testGasStorageMultiStageDist(shared_ptr< FullGrid > &p_grid, const double &p_maxLevelStorage, const int   &p_nbPeriodInTransition, const bool &p_bOneFile)
{
    boost::mpi::communicator world;

    // storage
    /////////
    double injectionRateStorage = 60000 / p_nbPeriodInTransition;
    double withdrawalRateStorage = 45000 / p_nbPeriodInTransition;
    double injectionCostStorage = 0.35;
    double withdrawalCostStorage = 0.35;

    double maturity = 1.;
    size_t nstep = 100;
    // define  a time grid
    shared_ptr<OneDimRegularSpaceGrid> timeGrid = make_shared<OneDimRegularSpaceGrid>(0., maturity / nstep, nstep);
    // future values
    shared_ptr<vector< double > > futValues = make_shared<vector<double> >(nstep + 1);
    // periodicity factor
    int iPeriod = 52;
    for (size_t i = 0; i < nstep + 1; ++i)
        (*futValues)[i] = 50. + 20 * sin((M_PI * i * iPeriod) / nstep);
    // define the future curve
    shared_ptr<OneDimData<OneDimRegularSpaceGrid, double> > futureGrid = make_shared<OneDimData< OneDimRegularSpaceGrid, double> >(timeGrid, futValues);
    // one dimensional factors
    int nDim = 1;
    VectorXd sigma = VectorXd::Constant(nDim, 0.94);
    VectorXd mr = VectorXd::Constant(nDim, 0.29);
    // number of simulations
    size_t nbsimulOpt = 80000;

    // a backward simulator
    ///////////////////////
    bool bForward = false;
    double r = 0.; // interest rate
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > backSimulator = make_shared<MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > >(futureGrid, sigma, mr, r, maturity, nstep, nbsimulOpt, bForward, p_nbPeriodInTransition);
    // optimizer
    ///////////
    shared_ptr< OptimizeGasStorageMultiStage< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > > storage = make_shared< OptimizeGasStorageMultiStage< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > >(injectionRateStorage, withdrawalRateStorage, injectionCostStorage, withdrawalCostStorage);
    // regressor
    ///////////
    int nMesh = 12;
    ArrayXi nbMesh = ArrayXi::Constant(1, nMesh);
    shared_ptr< BaseRegression > regressor = make_shared<LocalLinearRegression>(nbMesh);
    // final value
    function<double(const int &, const ArrayXd &, const ArrayXd &)>  vFunction = ZeroFunction();

    // initial values
    ArrayXd initialStock = ArrayXd::Constant(1, p_maxLevelStorage);
    int initialRegime = 0; // only one regime

    // Optimize
    ///////////
    string fileToDump = "CondExpGasStorageMultiStageDist";
    double valueOptim ;
    {
        // link the simulations to the optimizer
        storage->setSimulator(backSimulator);
        boost::timer::auto_cpu_timer t;

        valueOptim =  DynamicProgrammingByRegressionMultiStageDist(p_grid, storage, regressor, vFunction, initialStock, initialRegime, fileToDump, p_bOneFile,  world);
    }
    world.barrier();

    if (world.rank() == 0)
        cout << "valueOptim " << valueOptim << endl ;

    // a forward simulator
    ///////////////////////
    int nbsimulSim = 80000;
    bForward = true;
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > forSimulator = make_shared<MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > (futureGrid, sigma, mr, r, maturity, nstep, nbsimulSim, bForward, p_nbPeriodInTransition);
    double valSimu ;
    {
        // link the simulations to the optimizer
        storage->setSimulator(forSimulator);
        boost::timer::auto_cpu_timer t;
        valSimu = SimulateMultiStageRegressionDist(p_grid, storage, vFunction, initialStock, initialRegime, fileToDump, p_bOneFile,  world) ;

    }
    if (world.rank() == 0)
        cout << " valSimu  " << valSimu << " valueOptim " << valueOptim << endl ;
    if (world.rank() == 0)
        BOOST_CHECK_CLOSE(valueOptim, valSimu, accuracyClose);
    world.barrier();
}



BOOST_AUTO_TEST_CASE(testSimpleStorageMultiStageDistOneFile)
{
    // storage
    /////////
    double maxLevelStorage  = 90000;
    // grid
    //////
    int nGrid = 10;
    ArrayXd lowValues = ArrayXd::Constant(1, 0.);
    ArrayXd step = ArrayXd::Constant(1, maxLevelStorage / nGrid);
    ArrayXi nbStep = ArrayXi::Constant(1, nGrid);
    shared_ptr<FullGrid> grid = make_shared<RegularSpaceGrid>(lowValues, step, nbStep);
    // number of period at each transition step
    int nbPeriodInTransition = 3 ;
    bool bOneFile = true;
    testGasStorageMultiStageDist(grid, maxLevelStorage, nbPeriodInTransition, bOneFile);
}

BOOST_AUTO_TEST_CASE(testSimpleStorageMultiStageDistMultiFile)
{
    // storage
    /////////
    double maxLevelStorage  = 90000;
    // grid
    //////
    int nGrid = 10;
    ArrayXd lowValues = ArrayXd::Constant(1, 0.);
    ArrayXd step = ArrayXd::Constant(1, maxLevelStorage / nGrid);
    ArrayXi nbStep = ArrayXi::Constant(1, nGrid);
    shared_ptr<FullGrid> grid = make_shared<RegularSpaceGrid>(lowValues, step, nbStep);
    // number of period at each transition step
    int nbPeriodInTransition = 3 ;
    bool bOneFile = false;
    testGasStorageMultiStageDist(grid, maxLevelStorage, nbPeriodInTransition, bOneFile);
}

/// \brief valorization of a given gas storage on a  set of grids with n period
///        The characteristics of the cavity change with time.
/// \param p_timeChangeGrid       date for changing grids
/// \param p_grids                grids
/// \param p_maxLevelStorage  maximum level
/// \param p_nbPeriodInTransition  number of period at each transition step
void testGasStorageMultiStageVaryingDist(const vector<double>    &p_timeChangeGrid,  const vector<shared_ptr<FullGrid> >   &p_grids, const double &p_maxLevelStorage, const int   &p_nbPeriodInTransition)
{
#ifdef USE_MPI
    boost::mpi::communicator world;
#endif

    // storage
    /////////
    double injectionRateStorage = 60000 / p_nbPeriodInTransition;
    double withdrawalRateStorage = 45000 / p_nbPeriodInTransition;
    double injectionCostStorage = 0.35;
    double withdrawalCostStorage = 0.35;

    double maturity = 1.;
    size_t nstep = 100;
    // define  a time grid
    shared_ptr<OneDimRegularSpaceGrid> timeGrid(new OneDimRegularSpaceGrid(0., maturity / nstep, nstep));
    // future values
    shared_ptr<vector< double > > futValues(new vector<double>(nstep + 1));
    // periodicity factor
    int iPeriod = 52;
    for (size_t i = 0; i < nstep + 1; ++i)
        (*futValues)[i] = 50. + 20 * sin((M_PI * i * iPeriod) / nstep);
    // define the future curve
    shared_ptr<OneDimData<OneDimRegularSpaceGrid, double> > futureGrid(new OneDimData< OneDimRegularSpaceGrid, double> (timeGrid, futValues));
    // one dimensional factors
    int nDim = 1;
    VectorXd sigma = VectorXd::Constant(nDim, 0.94);
    VectorXd mr = VectorXd::Constant(nDim, 0.29);
    // number of simulations
    size_t nbsimulOpt = 80000;

    // no actualization
    double r  = 0. ;
    // a backward simulator
    ///////////////////////
    bool bForward = false;
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > backSimulator = make_shared<MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > >(futureGrid, sigma, mr, r, maturity, nstep, nbsimulOpt, bForward, p_nbPeriodInTransition);
    // optimizer
    ///////////
    shared_ptr< OptimizeGasStorageMultiStage< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > > storage(new  OptimizeGasStorageMultiStage< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > >(injectionRateStorage, withdrawalRateStorage, injectionCostStorage, withdrawalCostStorage));
    // regressor
    ///////////
    int nMesh = 8;
    ArrayXi nbMesh = ArrayXi::Constant(1, nMesh);
    shared_ptr< BaseRegression > regressor(new LocalLinearRegression(nbMesh));
    // final value
    function<double(const int &, const ArrayXd &, const ArrayXd &)>  vFunction = ZeroFunction();

    // initial values
    ArrayXd initialStock = ArrayXd::Constant(1,  p_maxLevelStorage);
    int initialRegime = 0; // only one regime

    // Optimize
    ///////////
    string fileToDump = "CondExpGasStorageVaryingCavMultiStageDist";
    double valueOptim ;
    bool bOneFile = true;
    {
        // link the simulations to the optimizer
        storage->setSimulator(backSimulator);
        boost::timer::auto_cpu_timer t;

        valueOptim =  DynamicProgrammingByRegressionVaryingGridsMultiStageDist(p_timeChangeGrid, p_grids, storage, regressor, vFunction,
                      initialStock, initialRegime, fileToDump, bOneFile, world);
        if (world.rank() == 0)
            cout << " ValueOptim " << valueOptim << endl ;
    }
    world.barrier();
    // a forward simulator
    ///////////////////////
    int nbsimulSim = 80000;
    bForward = true;
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > forSimulator(new	  MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > (futureGrid, sigma, mr, r, maturity, nstep, nbsimulSim, bForward, p_nbPeriodInTransition));
    double valSimu ;
    {
        // link the simulations to the optimizer
        storage->setSimulator(forSimulator);
        boost::timer::auto_cpu_timer t;
        valSimu = SimulateMultiStageRegressionVaryingGridsDist(p_timeChangeGrid, p_grids, storage, vFunction, initialStock, initialRegime,
                  fileToDump, bOneFile,  world) ;

    }
    if (world.rank() == 0)
    {
        cout << " valSimu  " << valSimu << " valueOptim " << valueOptim << endl ;
        BOOST_CHECK_CLOSE(valueOptim, valSimu, accuracyClose);
    }

}


BOOST_AUTO_TEST_CASE(testSimpleStorageVaryingCavityMultiStageDist)
{
    // storage
    /////////
    double maxLevelStorage  = 90000;
    // define changing grids
    vector<double> timeChangeGrid;
    timeChangeGrid.reserve(3);
    vector<shared_ptr<FullGrid> > grids;
    grids.reserve(3);
    // grids
    ////////
    int nGrid = 10;
    ArrayXd lowValues1 = ArrayXd::Constant(1, 0.);
    ArrayXd step1 = ArrayXd::Constant(1, maxLevelStorage / nGrid);
    ArrayXi nbStep1 = ArrayXi::Constant(1, nGrid);
    // first grid
    timeChangeGrid.push_back(0.);
    grids.push_back(make_shared<RegularSpaceGrid>(lowValues1, step1, nbStep1));
    ArrayXd lowValues2 = ArrayXd::Constant(1, 30000.);
    ArrayXd step2 = ArrayXd::Constant(1, 10000.);
    ArrayXi nbStep2 = ArrayXi::Constant(1, 3);
    // second grid
    timeChangeGrid.push_back(0.3);
    grids.push_back(make_shared<RegularSpaceGrid>(lowValues2, step2, nbStep2));
    ArrayXd lowValues3 = ArrayXd::Constant(1, 0.);
    ArrayXd step3 = ArrayXd::Constant(1, 15000.);
    ArrayXi nbStep3 = ArrayXi::Constant(1, 6);
    timeChangeGrid.push_back(0.7);
    grids.push_back(make_shared<RegularSpaceGrid>(lowValues3, step3, nbStep3));

    // number of period at each transition step
    int nbPeriodInTransition = 2 ;
    testGasStorageMultiStageVaryingDist(timeChangeGrid, grids, maxLevelStorage, nbPeriodInTransition);
}


// (empty) Initialization function. Can't use testing tools here.
bool init_function()
{
    return true;
}

int main(int argc, char *argv[])
{
#if defined   __linux
    enable_abort_on_floating_point_exception();
#endif
    boost::mpi::environment env(argc, argv);
    return ::boost::unit_test::unit_test_main(&init_function, argc, argv);
}
