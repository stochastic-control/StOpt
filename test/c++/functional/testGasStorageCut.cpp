// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef USE_MPI
#define BOOST_TEST_MODULE testGasStorageCut
#endif
#define BOOST_TEST_DYN_LINK
#ifdef USE_MPI
#include <boost/mpi.hpp>
#endif
#define _USE_MATH_DEFINES
#include <math.h>
#include <functional>
#include <memory>
#include <boost/test/unit_test.hpp>
#include <boost/timer/timer.hpp>
#include <Eigen/Dense>
#include "StOpt/core/grids/OneDimRegularSpaceGrid.h"
#include "StOpt/core/grids/OneDimData.h"
#include "StOpt/regression/LocalLinearRegression.h"
#include "StOpt/core/grids/RegularSpaceGridGeners.h"
#include "StOpt/core/grids/RegularLegendreGridGeners.h"
#include "StOpt/regression/LocalLinearRegressionGeners.h"
#include "test/c++/tools/simulators/MeanRevertingSimulator.h"
#include "test/c++/tools/dp/DynamicProgrammingByRegressionCut.h"
#include "test/c++/tools/dp/SimulateRegressionCut.h"
#include "test/c++/tools/dp/OptimizeGasStorageCut.h"

using namespace std;
using namespace Eigen ;
using namespace StOpt;

double accuracyClose = 2.;
double accuracyEqual = 0.0001;


class ZeroFunction
{
private :
    int m_nDim;
public:
    ZeroFunction(const int &p_nDim): m_nDim(p_nDim) {}
    ArrayXd  operator()(const int &, const ArrayXd &, const ArrayXd &) const
    {
        return ArrayXd::Zero(m_nDim) ;
    }
};



#if defined   __linux
#include <fenv.h>
#define enable_abort_on_floating_point_exception() feenableexcept(FE_DIVBYZERO | FE_INVALID)
#endif

template< class Grid>
void   testGasStorageCut(shared_ptr< Grid > &p_grid, const double &p_maxLevelStorage)
{
#ifdef USE_MPI
    boost::mpi::communicator world;
#endif
    // storage
    /////////
    double injectionRateStorage = 20000;
    double withdrawalRateStorage = 15000;
    double injectionCostStorage = 0.35;
    double withdrawalCostStorage = 0.35;

    double maturity = 1.;
    size_t nstep = 20;
    // define  a time grid
    shared_ptr<OneDimRegularSpaceGrid> timeGrid = make_shared< OneDimRegularSpaceGrid>(0., maturity / nstep, nstep);
    // future values
    shared_ptr<vector< double > > futValues(new vector<double>(nstep + 1));
    // periodicity factor
    int iPeriod = 52;
    for (size_t i = 0; i < nstep + 1; ++i)
        (*futValues)[i] = 50. + 5 * sin((M_PI * i * iPeriod) / nstep);
    // define the future curve
    shared_ptr<OneDimData<OneDimRegularSpaceGrid, double> > futureGrid = make_shared< OneDimData< OneDimRegularSpaceGrid, double> >(timeGrid, futValues);
    // one dimensional factors
    int nDim = 1;
    VectorXd sigma = VectorXd::Constant(nDim, 0.94);
    //VectorXd sigma = VectorXd::Constant(nDim, 0.00001);
    VectorXd mr = VectorXd::Constant(nDim, 0.29);
    // number of simulations
    size_t nbsimulOpt = 8000;
    // no actualization
    double r = 0 ;
    // a backward simulator
    ///////////////////////
    bool bForward = false;
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > backSimulator =   make_shared<MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > (futureGrid, sigma, mr, r, maturity, nstep, nbsimulOpt, bForward);
    // optimizer
    ///////////
    shared_ptr< OptimizeGasStorageCut< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> >  > > storage =
        make_shared<  OptimizeGasStorageCut< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> >  > > (injectionRateStorage, withdrawalRateStorage, injectionCostStorage, withdrawalCostStorage);

    // regressor
    ///////////
    int nMesh = 5;
    ArrayXi nbMesh = ArrayXi::Constant(1, nMesh);
    shared_ptr< BaseRegression > regressor(new LocalLinearRegression(nbMesh));
    // final value
    function< ArrayXd(const int &, const ArrayXd &, const ArrayXd &)>   vFunction = ZeroFunction(2);

    // initial values
    ArrayXd initialStock = ArrayXd::Constant(1, p_maxLevelStorage);
    int initialRegime = 0; // only one regime

    // Optimize
    ///////////
    string fileToDump = "CondCut";
    // link the simulations to the optimizer
    storage->setSimulator(backSimulator);
    double valueOptim =  DynamicProgrammingByRegressionCut(p_grid, storage, regressor, vFunction, initialStock, initialRegime, fileToDump
#ifdef USE_MPI
                         , world
#endif
                                                          );


    // a forward simulator
    ///////////////////////
    int nbsimulSim = 8000;
    bForward = true;
    shared_ptr< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > forSimulator = make_shared< MeanRevertingSimulator< OneDimData<OneDimRegularSpaceGrid, double> > > (futureGrid, sigma, mr, r, maturity, nstep, nbsimulSim, bForward);

    // link the simulations to the optimizer
    storage->setSimulator(forSimulator);
    double valSimu = SimulateRegressionCut(p_grid, storage, vFunction, initialStock, initialRegime, fileToDump
#ifdef USE_MPI
                                           , world
#endif
                                          ) ;

    BOOST_CHECK_CLOSE(valueOptim, valSimu, accuracyClose);
    cout << " Optim " << valueOptim << " valSimu " << valSimu <<  endl ;

}


BOOST_AUTO_TEST_CASE(testSimpleStorageCut)
{
    // storage
    /////////
    double maxLevelStorage  = 40000;
    // grid
    //////
    int nGrid = 10;
    ArrayXd lowValues = ArrayXd::Constant(1, 0.);
    ArrayXd step = ArrayXd::Constant(1, maxLevelStorage / nGrid);
    ArrayXi nbStep = ArrayXi::Constant(1, nGrid);
    shared_ptr<FullGrid> grid = make_shared<RegularSpaceGrid>(lowValues, step, nbStep);
    testGasStorageCut(grid, maxLevelStorage);

}



#ifdef USE_MPI
// (empty) Initialization function. Can't use testing tools here.
bool init_function()
{
    return true;
}

int main(int argc, char *argv[])
{
#if defined   __linux
    enable_abort_on_floating_point_exception();
#endif
    boost::mpi::environment env(argc, argv);
    return ::boost::unit_test::unit_test_main(&init_function, argc, argv);
}

#endif
