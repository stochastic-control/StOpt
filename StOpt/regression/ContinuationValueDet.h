// Copyright (C) 2023 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef CONTINUATIONVALUEDET_H
#define CONTINUATIONVALUEDET_H
#include <iostream>
#include <Eigen/Dense>
#include "StOpt/regression/ContinuationValueBase.h"
#include "StOpt/core/grids/SpaceGrid.h"

/** \file ContinuationValueDet.h
 *  \brief Calculate and store deterministic continuation value function
 *  \author Xavier Warin
 */

namespace StOpt
{

/// \class ContinuationValueDet ContinuationValueDet.h
/// Permits to calculate, store continuation values on a grid for regression methods
/// This deterministic continuation value is stored for many simulations
class ContinuationValueDet :  public ContinuationValueBase
{

private :
    Eigen::ArrayXXd m_cash ; ///< store cash on trajectories (nb of simulations, nb stock points)
    int m_nbSimul ; // simulation  number

public :
    /// \brief Default constructor
    ContinuationValueDet(): ContinuationValueBase() {}

    /// \brief Constructor
    /// \param p_grid   grid for stocks
    /// \param p_cash     matrix to store cash  (number of simulations by nb of stocks)
    ContinuationValueDet(const  std::shared_ptr< SpaceGrid >   &p_grid,
                         const Eigen::ArrayXXd &p_cash) :
        ContinuationValueBase(p_grid),  m_cash(p_cash), m_nbSimul(p_cash.rows())
    {}


    /// \brief Load another Continuation value object
    ///  Only a partial load of the objects is achieved
    /// \param p_grid   Grid to load
    /// \param p_values coefficient polynomials for regression
    virtual void loadForSimulation(const  std::shared_ptr< SpaceGrid > &p_grid,
                                   const Eigen::ArrayXXd &p_values)
    {
        m_grid = p_grid;
        m_cash = p_values ;
    }


    /// \brief Get all simulations conditional expectation
    /// \param p_ptOfStock   grid point for interpolation
    /// \return the continuation value associated to each simulation used in optimization
    Eigen::ArrayXd getAllSimulations(const Eigen::ArrayXd &p_ptOfStock) const
    {
        return m_grid->createInterpolator(p_ptOfStock)->applyVec(m_cash);
    }

    /// \brief Same as before but use an interpolator
    Eigen::ArrayXd getAllSimulations(const Interpolator   &p_interpol) const
    {
        return p_interpol.applyVec(m_cash);
    }

    /// \brief Get a conditional expectation for a simulation
    /// \param p_isim    simulation number
    /// \param  p_ptOfStock     stock points
    /// \return the continuation value associated to the given simulation used in optimization
    double  getASimulation(const int &p_isim, const Eigen::ArrayXd &p_ptOfStock) const
    {
        return m_grid->createInterpolator(p_ptOfStock)->apply(m_cash.row(p_isim).transpose());
    }


    /// \brief Same as before but use an interpolator
    /// \param p_isim         simulation number
    /// \param p_interpol     interpolator
    /// \return the continuation value associated to the given simulation used in optimization
    double  getASimulation(const int &p_isim, const Interpolator   &p_interpol) const
    {
        return p_interpol.apply(m_cash.row(p_isim).transpose());
    }

    //// \brief Get back
    ///@{
    const Eigen::ArrayXXd &getValues()  const
    {
        return m_cash;
    }
    Eigen::ArrayXXd getValuesCopy()  const
    {
        return m_cash;
    }

    inline int getNbSimul() const
    {
        return m_nbSimul ;
    }

    std::shared_ptr< SpaceGrid > getGrid() const
    {
        return m_grid;
    }

    ///@}

};
}
#endif /* CONTINUATIONVALUEDET_H */
