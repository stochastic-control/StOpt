// Copyright (C) 2023 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef CONTINUATIONVALUEBASE_H
#define CONTINUATIONVALUEBASE_H
#include <iostream>
#include <Eigen/Dense>
#include "StOpt/core/grids/SpaceGrid.h"

/** \file ContinuationValueBase.h
 *  \brief Base object for  continuation values objects  both deterministic and stochastic
 *  \author Xavier Warin
 */

namespace StOpt
{
///  \class ContinuationValueBase ContinuationValue.h
/// Base class for continuation object
class ContinuationValueBase
{
protected :
    std::shared_ptr< SpaceGrid >    m_grid ; ///< grid used to define stock points

public :
    /// \brief Default constructor
    ContinuationValueBase() {}

    /// \brief Constructor
    /// \param p_grid   grid for stocks
    ContinuationValueBase(const  std::shared_ptr< SpaceGrid >   &p_grid) :
        m_grid(p_grid)
    {}

    /// \brief Get all simulations conditional expectation
    /// \param p_ptOfStock   grid point for interpolation
    /// \return the continuation value associated to each simulation used in optimization
    virtual Eigen::ArrayXd getAllSimulations(const Eigen::ArrayXd &p_ptOfStock) const = 0;

    /// \brief Same as before but use an interpolator
    virtual Eigen::ArrayXd getAllSimulations(const Interpolator   &p_interpol) const = 0;

    /// \brief Get continuation vale  for a simulation
    /// \param p_isim    simulation number
    /// \param  p_ptOfStock     stock points
    /// \return the continuation value associated to the given simulation used in optimization
    virtual double  getASimulation(const int &p_isim, const Eigen::ArrayXd &p_ptOfStock) const = 0;


    /// \brief Same as before but use an interpolator
    /// \param p_isim         simulation number
    /// \param p_interpol     interpolator
    /// \return the continuation value associated to the given simulation used in optimization
    virtual double  getASimulation(const int &p_isim, const Interpolator   &p_interpol) const = 0;

    virtual std::shared_ptr< SpaceGrid > getGrid() const = 0;

    virtual inline int getNbSimul() const = 0;
    ///@}

};
}
#endif  /* CONTINUATIONVALUEBASE_H */

