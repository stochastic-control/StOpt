// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef  CONTINUATIONCUTSGENERS_H
#define  CONTINUATIONCUTSGENERS_H
#include <Eigen/Dense>
#include "geners/GenericIO.hh"
#include "StOpt/regression/ContinuationCuts.h"
#include "StOpt/regression/BaseRegressionGeners.h"
#include "StOpt/regression/LocalLinearRegressionGeners.h"
#include "StOpt/core/grids/SpaceGridGeners.h"
#include "StOpt/core/grids/RegularSpaceGridGeners.h"
#include "StOpt/core/grids/GeneralSpaceGridGeners.h"
#include "StOpt/core/utils/eigenGeners.h"

/** \file ContinuationCutsGeners.h
 * \brief Define non intrusive serialization with random access
*  \author Xavier Warin
 */

/// specialize the ClassIdSpecialization template
/// so that a ClassId object can be associated with the class we want to
/// serialize.  The second argument is the version number.
///@{
gs_specialize_class_id(StOpt::ContinuationCuts, 1)
/// an external class
gs_declare_type_external(StOpt::ContinuationCuts)
///@}

namespace gs
{
//
/// \brief  This is how the specialization of GenericWriter should look like
//
template <class Stream, class State >
struct GenericWriter < Stream, State, StOpt::ContinuationCuts,
           Int2Type<IOTraits<int>::ISEXTERNAL> >
{
    inline static bool process(const StOpt::ContinuationCuts  &p_regression, Stream &p_os,
                               State *, const bool p_processClassId)
    {
        // If necessary, serialize the class id
        static const ClassId current(ClassId::makeId<StOpt::ContinuationCuts >());
        const bool status = p_processClassId ? ClassId::makeId<StOpt::ContinuationCuts >().write(p_os) : true;
        // Serialize object data if the class id was successfully
        // written out
        if (status)
        {
            const Eigen::Array< Eigen::ArrayXXd, Eigen::Dynamic, 1  >   &values = p_regression.getValues();
            int isizeVal = values.size();
            write_pod(p_os, isizeVal);
            for (int ic = 0; ic < isizeVal; ++ic)
                write_item(p_os, values(ic));
            std::shared_ptr< StOpt::SpaceGrid > ptrGrid = p_regression.getGrid();
            bool bSharedPtr = (ptrGrid ? true : false);
            write_pod(p_os, bSharedPtr);
            if (bSharedPtr)
                write_item(p_os, *p_regression.getGrid());
            write_item(p_os, *p_regression.getCondExp());
        }
        // Return "true" on success, "false" on failure
        return status && !p_os.fail();
    }
};

/// \brief  And this is the specialization of GenericReader
//
template <class Stream, class State  >
struct GenericReader < Stream, State, StOpt::ContinuationCuts, Int2Type<IOTraits<int>::ISEXTERNAL> >
{
    inline static bool readIntoPtr(StOpt::ContinuationCuts  *&ptr, Stream &p_is,
                                   State *p_st, const bool p_processClassId)
    {

        if (p_processClassId)
        {
            static const ClassId current(ClassId::makeId<StOpt::ContinuationCuts>());
            ClassId id(p_is, 1);
            current.ensureSameName(id);
        }

        // Deserialize object data.
        int isizeVal = 0;
        read_pod(p_is, &isizeVal);
        Eigen::Array< Eigen::ArrayXXd, Eigen::Dynamic, 1> valReg(isizeVal);
        for (int ic = 0; ic < isizeVal ; ++ic)
        {
            std::unique_ptr<Eigen::ArrayXXd> valRegLoc  = read_item<Eigen::ArrayXXd>(p_is);
            valReg(ic) = *valRegLoc;
        }
        bool bSharedPtr ;
        read_pod(p_is, &bSharedPtr);
        std::unique_ptr<StOpt::SpaceGrid> pgrid ;
        if (bSharedPtr)
            pgrid  = read_item<StOpt::SpaceGrid>(p_is);
        std::shared_ptr<StOpt::SpaceGrid > pgridShared(std::move(pgrid));
        std::unique_ptr<StOpt::BaseRegression> pcond  = read_item<StOpt::BaseRegression>(p_is);
        std::shared_ptr<StOpt::BaseRegression> pcondShared(std::move(pcond));
        if (p_is.fail())
            // Return "false" on failure
            return false;
        //Build the object from the stored data
        if (ptr)
        {
            *ptr = StOpt::ContinuationCuts();
            ptr->loadForSimulation(pgridShared, pcondShared, valReg) ; // values);
            return true;
        }
        return false;
    }

    inline static bool process(StOpt::ContinuationCuts &s, Stream &is,
                               State *st, const bool p_processClassId)
    {
        // Simply convert reading by reference into reading by pointer
        StOpt::ContinuationCuts *ps = &s;
        return readIntoPtr(ps, is, st, p_processClassId);
    }
};
}



#endif/*  CONTINUATIONCUTSGENERS_H */
