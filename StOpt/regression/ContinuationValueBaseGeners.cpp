#include "StOpt/regression/BaseRegressionGeners.h"
// add include for all derived classes
//#include "StOpt/regression/ContinuationValueDetGeners.h"
#include "StOpt/regression/ContinuationValueGeners.h"

// Register all wrappers
SerializationFactoryForContinuationValueBase::SerializationFactoryForContinuationValueBase()
{
    //registerWrapper<ContinuationValueDetGeners>();
    registerWrapper<ContinuationValueGeners>();
}

