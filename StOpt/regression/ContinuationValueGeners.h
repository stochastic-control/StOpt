// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef  CONTINUATIONVALUEGENERS_H
#define  CONTINUATIONVALUEGENERS_H
#include <Eigen/Dense>
#include "geners/GenericIO.hh"
#include "StOpt/regression/ContinuationValue.h"
#include "StOpt/regression/ContinuationValueBaseGeners.h"
#include "StOpt/regression/BaseRegressionGeners.h"
#include "StOpt/regression/LocalLinearRegressionGeners.h"
#include "StOpt/regression/SparseRegressionGeners.h"
#include "StOpt/core/grids/SpaceGridGeners.h"
#include "StOpt/core/grids/RegularSpaceGridGeners.h"
#include "StOpt/core/grids/GeneralSpaceGridGeners.h"
#include "StOpt/core/grids/SparseSpaceGridNoBoundGeners.h"
#include "StOpt/core/grids/SparseSpaceGridBoundGeners.h"

/** \file ContinuationValueGeners.h
 * \brief Define non intrusive serialization with random access
*  \author Xavier Warin
 */

// Concrete reader/writer for class ContinuationValue
// Note publication of ContinuationValue as "wrapped_type".
struct ContinuationValueGeners: public gs::AbsReaderWriter<StOpt::ContinuationValueBase>
{
    typedef StOpt::ContinuationValueBase wrapped_base;
    typedef StOpt::ContinuationValue wrapped_type;

    // Methods that have to be overridden from the base
    bool write(std::ostream &, const wrapped_base &, bool p_dumpId) const override;
    wrapped_type *read(const gs::ClassId &p_id, std::istream &p_in) const override;

    // The class id forContinuationValue  will be needed both in the "read" and "write"
    // methods. Because of this, we will just return it from one static
    // function.
    static const gs::ClassId &wrappedClassId();
};

gs_specialize_class_id(StOpt::ContinuationValue, 1)
gs_declare_type_external(StOpt::ContinuationValue)
gs_associate_serialization_factory(StOpt::ContinuationValue, StaticSerializationFactoryForContinuationValueBase)

#endif/*  CONTINUATIONVALUEGENERS_H */
