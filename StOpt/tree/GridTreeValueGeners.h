// Copyright (C) 2019 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef  GRIDTREEVALUEGENERS_H
#define  GRIDTREEVALUEGENERS_H
#include <Eigen/Dense>
#include "geners/GenericIO.hh"
#include "StOpt/tree/GridTreeValue.h"
#include "StOpt/core/grids/SpaceGridGeners.h"
#include "StOpt/core/grids/RegularSpaceGridGeners.h"
#include "StOpt/core/grids/GeneralSpaceGridGeners.h"
#include "StOpt/core/grids/SparseSpaceGridNoBoundGeners.h"
#include "StOpt/core/grids/SparseSpaceGridBoundGeners.h"
#include "StOpt/core/grids/SparseInterpolatorSpectralGeners.h"
#include "StOpt/core/grids/InterpolatorSpectralGeners.h"
#include "StOpt/core/grids/LinearInterpolatorSpectralGeners.h"
#include "StOpt/core/grids/LegendreInterpolatorSpectralGeners.h"


/** \file GridTreeValueGeners.h
 * \brief Define non intrusive serialization with random access
*  \author Xavier Warin
 */

/// specialize the ClassIdSpecialization template
/// so that a ClassId object can be associated with the class we want to
/// serialize.  The second argument is the version number.
///@{
gs_specialize_class_id(StOpt::GridTreeValue, 1)
/// an external class
gs_declare_type_external(StOpt::GridTreeValue)
///@}

namespace gs
{
//
/// \brief  This is how the specialization of GenericWriter should look like
//
template <class Stream, class State >
struct GenericWriter < Stream, State, StOpt::GridTreeValue,
           Int2Type<IOTraits<int>::ISEXTERNAL> >
{
    inline static bool process(const StOpt::GridTreeValue  &p_state, Stream &p_os,
                               State *, const bool p_processClassId)
    {
        // If necessary, serialize the class id
        static const ClassId current(ClassId::makeId<StOpt::GridTreeValue >());
        const bool status = p_processClassId ? ClassId::makeId<StOpt::GridTreeValue >().write(p_os) : true;
        // Serialize object data if the class id was successfully
        // written out
        if (status)
        {
            std::shared_ptr< StOpt::SpaceGrid > ptrGrid = p_state.getGrid();
            bool bSharedPtr = (ptrGrid ? true : false);
            write_pod(p_os, bSharedPtr);
            if (bSharedPtr)
            {
                write_item(p_os, ptrGrid);
                write_item(p_os, p_state.getInterpolators());
            }
        }
        // Return "true" on success, "false" on failure
        return status && !p_os.fail();
    }
};

/// \brief  And this is the specialization of GenericReader
//
template <class Stream, class State  >
struct GenericReader < Stream, State, StOpt::GridTreeValue, Int2Type<IOTraits<int>::ISEXTERNAL> >
{
    inline static bool readIntoPtr(StOpt::GridTreeValue  *&ptr, Stream &p_is,
                                   State *, const bool p_processClassId)
    {

        if (p_processClassId)
        {
            static const ClassId current(ClassId::makeId<StOpt::GridTreeValue>());
            ClassId id(p_is, 1);
            current.ensureSameName(id);
        }

        // Deserialize object data.
        bool bSharedPtr ;
        read_pod(p_is, &bSharedPtr);
        std::shared_ptr<StOpt::SpaceGrid > pgridShared;
        CPP11_auto_ptr<std::vector< std::shared_ptr<StOpt::InterpolatorSpectral> > > pinterp;
        if (bSharedPtr)
        {
            CPP11_auto_ptr<StOpt::SpaceGrid> pgrid = read_item<StOpt::SpaceGrid>(p_is);
            pgridShared = std::move(pgrid);
            pinterp = read_item< std::vector< std::shared_ptr<StOpt::InterpolatorSpectral> > >(p_is);
            /// now affect grids to interpolator
            for (size_t i = 0 ; i < pinterp->size(); ++i)
                (*pinterp)[i]->setGrid(& *pgridShared);
        }

        if (p_is.fail())
            // Return "false" on failure
            return false;
        //Build the object from the stored data
        if (ptr)
        {
            if (bSharedPtr)
                *ptr = StOpt::GridTreeValue(pgridShared,  *pinterp);
            else
                *ptr = StOpt::GridTreeValue();
        }
        else
        {
            if (bSharedPtr)
                ptr = new  StOpt::GridTreeValue(pgridShared,  *pinterp);
            else
                ptr = new  StOpt::GridTreeValue();
        }
        return true;
    }

    inline static bool process(StOpt::GridTreeValue &s, Stream &is,
                               State *st, const bool p_processClassId)
    {
        // Simply convert reading by reference into reading by pointer
        StOpt::GridTreeValue *ps = &s;
        return readIntoPtr(ps, is, st, p_processClassId);
    }
};
}



#endif/*  GRIDANDREGRESSEDVALUEGENERS_H */
