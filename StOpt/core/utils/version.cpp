// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#include <boost/version.hpp>
#include <iostream>
#include <Eigen/Core>
#include "StOpt/core/utils/version.h"

using namespace std ;

namespace StOpt
{
std::string getStOptVersion()
{
    std::stringstream stOpt_version;

    stOpt_version << "StOpt version "
                  << STOPT_VERSION ;

    stOpt_version << ";  Boost version " << BOOST_VERSION / 100000 << "." << BOOST_VERSION / 100 % 1000 << "." << BOOST_VERSION % 100 << endl;

    stOpt_version << ";  Eigen version 3." << EIGEN_MAJOR_VERSION << "." << EIGEN_MINOR_VERSION ;

    return stOpt_version.str();
}
}
