// Copyright (C) 2016 EDF
// All Rights Reserved
// This code is published under the GNU Lesser General Public License (GNU LGPL)
#ifndef VERSION_H
#define VERSION_H
#define STOPT_VERSION "5.13"
#include <string>

/** \file version.h
 * \brief Defines StOpt version
 * \author Xavier Warin
 */

namespace StOpt
{
/// \brief get back library version
std::string getStOptVersion();
}
#endif
